<?php

namespace Lightnest\UserBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use Lightnest\UserBundle\Entity\User;
use Lightnest\UserBundle\Entity\Role;
use Lightnest\UserBundle\Entity\UserRepository;

/**
 * Form builder for User Entity
 *
 */
class UserType extends AbstractType
{	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class'		=> 'Lightnest\UserBundle\Entity\User',
			'validation_groups'	=> function(FormInterface $form){
				//Define if we use the Asserts of the registration or default group
				$data = $form->getData();
				if (!$form || $data->getId() === null){
					return array('registration');
				}
				return array('default');
			},
			'csrf_protection'	=> true,
			'csrf_field_name'	=> '_token',
			'intention'			=> 'user_form_2014',
		));
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{		
		$builder
			->add('username')
			->add('password')
			;
	}
	
	public function getName()
	{
		return 'user';
	}
}