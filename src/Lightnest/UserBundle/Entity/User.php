<?php

namespace Lightnest\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Lightnest\UserBundle\Entity\UserRepository;

/**
 * Lightnest\UserBundle\Entity\User
 * 
 * @ORM\Table(name="lightnest_users")
 * @ORM\Entity(repositoryClass="Lightnest\UserBundle\Entity\UserRepository")
 * @UniqueEntity("username", groups={"registration", "default"})
 */
class User implements UserInterface, \Serializable
{	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string", length=50, unique=true, nullable=false)
	 * @Assert\NotBlank()
	 * @Assert\Email(checkMX=true)
	 */
	private $username;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=false)
	 * @Assert\NotBlank(groups={"registration"})
	 * @Assert\Length(min="6", max="60", groups={"registration"})
	 */
	private $password;
    
	/**
	 * @ORM\Column(name="is_active", type="boolean")
	 */
	private $isActive;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Lightnest\UserBundle\Entity\Role", inversedBy="users")
	 * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
	 */
	private $role;
	
	/**
	 * @ORM\OneToMany(targetEntity="Lightnest\DistributionBundle\Entity\Report", mappedBy="user", cascade={"remove", "persist"}))
	 */
	private $files;
	
	/**
	 * @ORM\Column(name="must_change", type="boolean")
	 * 
	 * When set to true, force the user to change his password before getting access to the rest
	 */
	private $mustChange;
	
	public function __construct($mustChange = false)
	{
		$this->isActive = true;
		$this->mustChange = $mustChange;
		$this->files = new ArrayCollection();
	}
	
	/**
	 * @inheritDoc
	 */
	public function getUsername()
	{
		return $this->username;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getSalt()
	{
		return null;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getPassword()
	{
			return $this->password;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getRole()
	{
		return $this->role;
	}
	
	/**
	 * @inheritDoc
	 */
	public function setRole($role)
	{
		$this->role = $role;
		return $this;
	}
	
	/**
	 * @inheritDoc
	 */
	public function eraseCredentials()
	{
	}
	
	/**
	 * @see \Serializable::serialize()
	 */
	public function serialize()
	{
		return serialize(array(
			$this->id,
			$this->username,
			$this->password,
		));
	}
	
	/**
	 * @see \Serialiazable::unserialize()
	 */
	public function unserialize($serialized)
	{
			list(
				$this->id,
				$this->username,
				$this->password,
			) = unserialize($serialized);
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function getRoles()
    {    	
    	return array($this->role->getRole());
    }
    
    /**
     * get mustChange
     * 
     * @return boolean
     */
    public function getMustChange()
    {
    	return $this->mustChange;
    }
    
    /**
     * set mustHave
     * 
     * @param boolean $mustChange
     * @return User
     */
    public function setMustChange($mustChange)
    {
    	$this->mustChange = $mustChange;
    	return $this;
    }
	
	/**
	 * @return ArrayCollection
	 */
	public function getFiles()
	{
		return $this->files;
	}

    /**
     * Add file
     *
     * @param \Lightnest\DistributionBundle\Entity\Report $file
     * @return User
     */
    public function addFile(\Lightnest\DistributionBundle\Entity\Report $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Lightnest\DistributionBundle\Entity\Report $file
     * @return User
     */
    public function removeFile(\Lightnest\DistributionBundle\Entity\Report $file)
    {
        $this->files->removeElement($file);
        return $this;
    }
}
