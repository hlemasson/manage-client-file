<?php

namespace Lightnest\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="lightnest_role")
 * @ORM\Entity(repositoryClass="Lightnest\UserBundle\Entity\RoleRepository")
 */
class Role implements RoleInterface
{	
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id()
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\Column(name="name", type="string", length=30)
	 */
	private $name;
	
	/**
	 * @ORM\Column(name="role", type="string", length=30, unique=true, nullable=false)
	 */
	private $role;
	
	/**
	 * @ORM\Column(name="number", type="integer", nullable=false)
	 * 
	 * Weight the powerfull of the role
	 */
	private $number;
	
	/**
	 * @ORM\OneToMany(targetEntity="Lightnest\UserBundle\Entity\User", mappedBy="role")
	 */
	private $users;
	
	public function __construct()
	{
		$this->users = new ArrayCollection();
	}
	
	/**
	 * @see RoleInterface
	 */
	public function getRole()
	{
		return $this->role;
	}
	
	/**
	 * @see RoleInterface
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @see RoleInterface
	 */
	public function getUsers()
	{
		return $this->users;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \Lightnest\UserBundle\Entity\User $users
     * @return Role
     */
    public function addUser(\Lightnest\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Lightnest\UserBundle\Entity\User $users
     */
    public function removeUser(\Lightnest\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }
    
    public function __toString()
    {
    	return $this->role;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Role
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }
}
