<?php

namespace Lightnest\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository implements UserProviderInterface
{
	/**
	 * Define the logic for loading the user in the session by joining his roles and his files to avoid useless sql requests
	 * @param string $username
	 * @throws UsernameNotFoundException
	 */
	public function loadUserByUsername($username)
	{
		$q = $this->createQueryBuilder('u')
			->select('u', 'r')
			->leftJoin('u.role', 'r')
			->leftJoin('u.files', 'f')
			->where('u.username = :username')
			->setParameter('username', $username)
			->getQuery();
			
		try
		{
			//This method throws an exception if there is no record for the query
			$user = $q->getSingleResult();
		} catch(NoResultException $e)
		{
			$message = \sprintf('Unable to find a user identified by "%s".', $username);
			throw new UsernameNotFoundException($message, 0, $e);
		}
		
		return $user;
	}
	
	public function refreshUser(UserInterface $user)
	{
		$class = get_class($user);
		if (!$this->supportsClass($class))
		{
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class ||
        	is_subclass_of($class, $this->getEntityName());
    }
    
    /**
     *
     */
    public function getAllUsersWithJoinQuery()
    {
    	$q = $this->createQueryBuilder('u')
    		->select('u', 'r')
    		->leftJoin('u.role', 'r')
    		->getQuery();
    		
    	return $q;
    }
    
    /**
     * Get all users with their roles (with join) to avoid multiple sql requests
     */
    public function getAllUsersWithJoin()
    {
    	return $this->getAllUsersWithJoinQuery()->getResult();
    }
    
    /**
     * Get a specific user with his roles (with join) to avoid multiple sql requests
     * @param integer $id
     */
    public function getUserWithJoin($id)
    {
    	$q = $this->createQueryBuilder('u')
    		->select('u', 'r')
    		->leftJoin('u.role', 'r')
    		->where('u.id = :id')
    		->setParameter('id', $id)
    		->getQuery()
    		;
    	
    	$results = $q->getResult();
    	if (count($results) == 0)
    		return null;
    	return $results[0];
    }
    
    /**
     *
     */
    public function getAllClientsQuery()
    {
    	$q = $this->createQueryBuilder('u')
    		->select('u', 'r')
    		->leftJoin('u.role', 'r')
    		->where('r.number < 2')
    		->getQuery();
    		
    	return $q;
    }
    
    /**
     * 
     * Get all client users
     * @param string $role
     */
    public function getAllClients()
    {	
    	return $this->getAllClientsQuery()->getResult();
    }
    
    /**
     * Get a specific user with his roles and files
     */
    public function getUserWithFiles($id)
    {
    	$q = $this->createQueryBuilder('u')
    		->select('u', 'r', 'f')
    		->leftJoin('u.role', 'r')
    		->leftJoin('u.files', 'f')
    		->where('u.id = :id')
    		->setParameter('id', $id)
    		->getQuery();
    	
    	
    	$results = $q->getResult();
    	if (count($results) == 0)
    		return null;
    	return $results[0];
    }
}
