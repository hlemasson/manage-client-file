<?php

namespace Lightnest\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lightnest\UserBundle\Entity\Role;

/**
 * Load all needed Role Entities
 *
 */
class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$role_user = new Role();
		$role_user->setName("ROLE_USER");
		$role_user->setRole("ROLE_USER");
		$role_user->setNumber(0);
		
		$role_client = new Role();
		$role_client->setName("ROLE_CLIENT");
		$role_client->setRole("ROLE_CLIENT");
		$role_client->setNumber(1);
		
		$role_staff = new Role();
		$role_staff->setName("ROLE_STAFF");
		$role_staff->setRole("ROLE_STAFF");
		$role_staff->setNumber(2);
		
		$role_allowed_to_switch = new Role();
		$role_allowed_to_switch->setName("ROLE_ALLOWED_TO_SWITCH");
		$role_allowed_to_switch->setRole("ROLE_ALLOWED_TO_SWITCH");
		$role_allowed_to_switch->setNumber(3);
		
		$role_admin = new Role();
		$role_admin->setName("ROLE_ADMIN");
		$role_admin->setRole("ROLE_ADMIN");
		$role_admin->setNumber(4);
		
		
		$manager->persist($role_user);
		$manager->persist($role_client);
		$manager->persist($role_staff);
		$manager->persist($role_admin);
		$manager->persist($role_allowed_to_switch);
		$manager->flush();
		
		$this->addReference('role_user', $role_user);
		$this->addReference('role_client', $role_client);
		$this->addReference('role_staff', $role_staff);
		$this->addReference('role_admin', $role_admin);
		$this->addReference('role_allowed_to_switch', $role_allowed_to_switch);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 10;
	}
}
