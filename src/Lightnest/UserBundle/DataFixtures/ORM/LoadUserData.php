<?php

namespace Lightnest\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Lightnest\UserBundle\Entity\User;

/**
 * Load some records for User Entity
 * All Users but $user_admin could be deleted
 *
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
	/**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$user_admin = new User();
		$user_admin->setUsername("lemasson.h@gmail.com");
		
		$encoder = $this->container->get('security.encoder_factory')->getEncoder($user_admin);
		$password = $encoder->encodePassword('helene', $user_admin->getSalt());
		$user_admin->setPassword($password);
		
		$user_admin->setRole($this->getReference('role_admin'));
		
		//Staff Users
		$user_staff1 = new User();
		$user_staff1->setUsername("staff@gmail.com");
		
		$password = $encoder->encodePassword('lemascon', $user_staff1->getSalt());
		$user_staff1->setPassword($password);
		$user_staff1->setRole($this->getReference('role_staff'));
		
		
		//Client Users
		$user_client1 = new User();
		$user_client1->setUsername("client1@gmail.com");
		$password = $encoder->encodePassword('clientnumb1', $user_client1->getSalt());
		$user_client1->setPassword($password);
		$user_client1->setRole($this->getReference('role_client'));
		
		$user_client2 = new User();
		$user_client2->setUsername("client2@gmail.com");
		$password = $encoder->encodePassword('clientnumb2', $user_client2->getSalt());
		$user_client2->setPassword($password);
		$user_client2->setRole($this->getReference('role_client'));
		
		$manager->persist($user_admin);
		$manager->persist($user_staff1);
		$manager->persist($user_client1);
		$manager->persist($user_client2);
		$manager->flush();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 15;
	}
}