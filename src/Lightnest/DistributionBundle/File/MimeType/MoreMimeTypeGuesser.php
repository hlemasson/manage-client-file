<?php
namespace Lightnest\DistributionBundle\File\MimeType;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;

/**
 * Created to fix the bug of the microsoft mime type on uploaded file
 */
class MoreMimeTypeGuesser implements MimeTypeGuesserInterface
{
	private $magicFile;
	
	private static $arrayZips = array(
		"application/zip",
		"application/x-zip",
		"application/x-zip-compressed"
	);
 
	private static $arrayExtensions = array(
		".pptx",
		".docx",
		".dotx",
		".xlsx"
	);
	
	public function __construct($magicFile = null)
	{
		$this->magicFile = $magicFile;
	}
	
	public static function isSupported()
	{
		return function_exists('finfo_open');
	}
	
	/**
	 * 
 	 * Because of the system of MimeTypeGuesser, get a .tmp as a $path and can't associate to the good mime type
	 * @param string $path
	 * @throws FileNotFoundException
	 * @throws AccessDeniedException
	 */
	public function guess($path)
	{
		if (!is_file($path)){
			throw new FileNotFoundException($path);
		}
		
		if (!is_readable($path)){
			throw new AccessDeniedException($path);
		}
		
		if (!self::isSupported()){
			return null;
		}
		
		if (!$finfo = new \finfo(FILEINFO_MIME_TYPE, $this->magicFile)){
			return null;
		}
		
		$type = $finfo->file($path);
	
		$originalExtension = (false === $pos = strrpos($path, '.')) ? '' : substr($path, $pos);
		if (in_array($type, MoreMimeTypeGuesser::$arrayZips) && in_array($originalExtension, MoreMimeTypeGuesser::$arrayExtensions)){
		   return $originalExtension;
		}
		
		return $type;
	}
}