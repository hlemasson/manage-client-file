<?php

namespace Lightnest\DistributionBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Lightnest\DistributionBundle\Entity\Report;

/**
 * Form builder for the Report entity
 *
 */
class ReportType extends AbstractType
{	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class'		=> 'Lightnest\DistributionBundle\Entity\Report',
			'csrf_protection'	=> true,
			'csrf_field_name'	=> '_token',
			'intention'			=> 'file_form',
		));
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{		
		$builder
			->add('name')
			->add('file', 'file', array('required'=> true))
			;
	}
	
	public function getName()
	{
		return 'report';
	}
}