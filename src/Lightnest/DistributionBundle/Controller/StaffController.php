<?php
namespace Lightnest\DistributionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Lightnest\DistributionBundle\File\MimeType\MoreMimeTypeGuesser;

use Lightnest\UserBundle\Entity\User;
use Lightnest\UserBundle\Form\Type\UserType;
use Lightnest\UserBundle\Entity\UserRepository;
use Lightnest\DistributionBundle\Entity\Report;
use Lightnest\DistributionBundle\Form\Type\ReportType;

/**
 * @Route("/staff")
 */
class StaffController extends Controller
{
    /**
     * @Route("/", name="homepage_staff")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function indexAction()
    {
    	return array();
    }
    
    /**
     * @Route("/client/new", name="staff_user_new")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function newAction(Request $request)
    {
    	//Create form and set role to ROLE_CLIENT and set mustChange attribute to true
    	$em = $this->getDoctrine()->getManager();
    	$user = new User(true);
    	$role = $em->getRepository("LightnestUserBundle:Role")->findOneByRole("ROLE_CLIENT");
    	$user->setRole($role);
    	$form = $this->createForm(new UserType(), $user);
    	
    	$form->handleRequest($request);
    	
    	if ($form->isValid()) {
    		//Encode the password with the encoder define in config.yml
    		$encoder = $this->get('security.encoder_factory')->getEncoder($user);
    		$passwordNEncode = $user->getPassword();
    		$password = $encoder->encodePassword($passwordNEncode, $user->getSalt());
    		$user->setPassword($password);
    		
    		$em->persist($user);
    		$em->flush($user);
    		
    		try {
    			//Send email to the new user
    			$message = \Swift_Message::newInstance()
        			->setSubject('Welcome on Lightnest SEO Portal')
        			->setFrom('staff-lightnest-seo-portal@lightnest.com')
        			->setTo($user->getUsername())
        			->setContentType('text/html')
        			->setBody($this->renderView('LightnestDistributionBundle:Staff:emailNewUser.html.twig', array(
        				'user'		=> $user->getUsername(),
        				'password'	=> $passwordNEncode,
        				'host'		=> $this->container->getParameter('email_host')
	        		)))
	        	;
    			$this->get('mailer')->send($message);
    		} catch (Exception $e) {
    			error_log($e->getMessage());
    		}
    		
    		$this->get('session')->getFlashBag()->add(
            	'notice',
            	'The user ' . $user->getUsername() . ' has been created'
    		);
    		return $this->redirect($this->generateUrl('staff_user_list'), 301);
    	}
    	
    	return array(
    		'form' => $form->createView(),
    	);
    }
    
    /**
     * @Route("/client/{id}/delete", name="staff_user_delete")
     * @Security("has_role('ROLE_STAFF')")
     */
    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository("LightnestUserBundle:User")->getUserWithFiles($id);
    	if (!$user){
    		throw $this->createNotFoundException('Unable to find the User id: ' . $id . '.');
    	}
    	
    	//If user to delete is a staff account or higher, prohibit the removal
    	if ($user->getRole()->getNumber() >= 2){
    		$this->get('session')->getFlashBag()->add(
            	'warning',
            	'You can not delete an admin/staff account'
    		);
    		return $this->redirect($this->generateUrl('staff_user_list'), 301);
        	
    	}
    	$em->remove($user);
    	$this->get('session')->getFlashBag()->add(
           	'notice',
           	'The user ' . $user->getUsername() . ' has been deleted.'
    	);
    	$em->flush();
    	
    	return $this->redirect($this->generateUrl('staff_user_list'), 301);
    }
    
    /**
     * @Route("/client/list", name="staff_user_list")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function listAction()
    {    	
    	$query = $this->getDoctrine()->getRepository('LightnestUserBundle:User')->getAllClientsQuery();
    	
    	$paginator  = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
        	$query,
        	$this->get('request')->query->get('page', 1)/*page number*/,
        	10/*limit per page*/
    	);
    	
    	return array(
    		'pagination' => $pagination
    	);
    }
    
    /**
     * @Route("/client/{id}/file/new", name="staff_user_file_new")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function newFileAction($id, Request $request)
    {
    	//Should fix bug for .docx
    	$guesser = MimeTypeGuesser::getInstance();
 		$guesser->register(new MoreMimeTypeGuesser());
 		
    	$em = $this->getDoctrine()->getManager();
    	
    	//Check the right to add a file to user with $id
    	$user = $em->getRepository("LightnestUserBundle:User")->getUserWithJoin($id);
    	if (!$user){
    		throw $this->createNotFoundException('Unable to find the User id: ' . $id . '.');
    	}
    	if ($user->getRole()->getNumber() >= 2){
    		$this->get('session')->getFlashBag()->add(
            	'warning',
            	'You cannot add a file to an admin/staff user'
    		);
    		return $this->redirect($this->generateUrl('homepage_staff'), 301);
    	}
    	
    	//Create file form
    	$file = new Report();
    	$form = $this->createForm(new ReportType(), $file);
    	$form->handleRequest($request);
    	
    	$fileupload = $file->getFile();
    	
    	//If post method, validate/save the file
    	if ($form->isValid()) {
    		$file->setUser($user);
    		$em->persist($file);
    		$em->flush();
    		
    		try {
    			//Send email to the client to inform him of a new file
    			$message = \Swift_Message::newInstance()
        			->setSubject('New file on Lightnest SEO Portal')
        			->setFrom('staff-lightnest-seo-portal@lightnest.com')
        			->setTo($user->getUsername())
        			->setContentType('text/html')
        			->setBody($this->renderView('LightnestDistributionBundle:Staff:emailUploadedFile.html.twig', array(
        				'user'	=> $user->getUsername(),
        				'file'	=> $file->getName(),
        				'host'		=> $this->container->getParameter('email_host')
	        		)))
	        	;
    			$this->get('mailer')->send($message);
    		} catch (Exception $e) {
    			error_log($e->getMessage());
    		}
    		
    		$this->get('session')->getFlashBag()->add(
            	'notice',
            	'The file ' . $file->getName() . ' has been uploaded'
    		);
    		return $this->redirect($this->generateUrl('staff_user_file_list', array('id' => $id)), 301);
    	}
    	
    	return array(
    		'user_file'	=> $user,
    		'form'		=> $form->createView(),
    	);
    }
    
    /**
     * @Route("/client/{id}/file", name="staff_user_file_list")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function listFileAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	//Check if the user with $id can have files, else redirect the actual user to the homepage_staff
    	$user = $em->getRepository("LightnestUserBundle:User")->getUserWithFiles($id);
    	if (!$user){
    		throw $this->createNotFoundException('Unable to find the User id: ' . $id . '.');
    	}
    	if ($user->getRole()->getNumber() >= 2){
    		$this->get('session')->getFlashBag()->add(
            	'warning',
            	'An admin/staff user doesn\'t have files'
    		);
    		return $this->redirect($this->generateUrl('homepage_staff'), 301);
    	}
    	
    	$paginator  = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
        	$em->getRepository('LightnestDistributionBundle:Report')->getFilesQuery($user->getId()),
        	$this->get('request')->query->get('page', 1)/*page number*/,
        	10/*limit per page*/
    	);
    	
    	return array(
    		'user_file'		=> $user,
    		'pagination'	=> $pagination,
    	);
    }
    
    /**
     * @Route("/client/file/{id}/delete", name="staff_user_file_delete")
     * @Template()
     * @Security("has_role('ROLE_STAFF')")
     */
    public function deleteFileAction($id)
    {
    	//Check if file with $id exists
    	$em = $this->getDoctrine()->getManager();
    	$file = $em->getRepository("LightnestDistributionBundle:Report")->getFileWithUser($id);
    	
    	if (!$file){
    		throw $this->createNotFoundException('Unable to find the file with id: ' . $id . '.');
    	}
    	
    	//Save information to inform of the success of the action on the user
    	$user = $file->getUser();
    	$name = $file->getName();
    	$em->remove($file);
    	$em->flush();
    	
    	$this->get('session')->getFlashBag()->add(
           	'notice',
           	'The file ' . $name . ' has been deleted.'
    	);
    	return $this->redirect($this->generateUrl('staff_user_file_list', array('id' => $user->getId())), 301);
    }
}