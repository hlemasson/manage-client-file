<?php

namespace Lightnest\DistributionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage_main")
     * @Template()
     */
    public function indexAction()
    {
    	//Redirect the user on his first homepage depends on his roles
    	$security_context = $this->get('security.context');
    	if ($security_context->isGranted('ROLE_ADMIN')) {
    		return $this->redirect($this->generateUrl('homepage_admin'), 301);
    	}
    	else if ($security_context->isGranted('ROLE_STAFF')) {
    		return $this->redirect($this->generateUrl('homepage_staff'), 301);
    	}
    	else if ($security_context->isGranted('ROLE_CLIENT')) {
    		return $this->redirect($this->generateUrl('homepage_client'), 301);
    	}
    	else {
    		throw new AccessDeniedException();
    	}
        return array();
    }
    
    /**
     * @Route("/reset-password", name="user_change_password")
     * @Template()
     * @Security("has_role('ROLE_CLIENT')")
     */
    public function changePasswordAction(Request $request)
    {
    	//Create form to reset the user password
    	$form = $this->createFormBuilder()
    		->add('password', 'repeated', array(
    			'type' => 'password',
    			'invalid_message' => 'The passwords must be identicals',
    			'options' => array(
    				'required' => true,
    				'max_length' => 20,
    			),
    			'first_options'  => array('label' => 'New Password'),
    			'second_options' => array('label' => 'Confirm Password'),
			))
    		->getForm();
    	
    	//Performs bind if this is a POST request
    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		$data = $form->getData();
    		
    		//Encode password and assigns to the current user
    		$user = $this->getUser();
    		$encoder = $this->get('security.encoder_factory')->getEncoder($user);
    		$password = $encoder->encodePassword($data['password'], $user->getSalt());
    		$user->setPassword($password);
    		$user->setMustChange(false);
    		
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($user);
    		$em->flush($user);
    		
    		//Set Information for informing the user of the action
    		$this->get('session')->getFlashBag()->add(
            	'notice',
            	'Your password have been updated'
    		);
    		return $this->redirect($this->generateUrl('homepage_client'), 301);
    	}
    		
    	return array(
    		'form'	=> $form->createView()
    	);
    }
}
