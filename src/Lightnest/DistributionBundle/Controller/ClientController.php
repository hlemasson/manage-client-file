<?php

namespace Lightnest\DistributionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * @Route("/", name="homepage_client")
     * @Template()
     * @Security("has_role('ROLE_CLIENT')")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $this->getUser();
    	
    	$paginator  = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
        	$em->getRepository('LightnestDistributionBundle:Report')->getFilesQuery($user->getId()),
        	$this->get('request')->query->get('page', 1)/*page number*/,
        	10/*limit per page*/
    	);
    	
    	return array(
    		'user_file'		=> $user,
    		'pagination'	=> $pagination
    	);
    }
    
    /**
     * 
     * @Route("/file/{id}/download", name="client_file_download")
     * @Security("has_role('ROLE_CLIENT')")
     */
	public function downloadFileAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $this->getUser();
    	
    	//Get & Check if the file exists
    	$file = $em->getRepository("LightnestDistributionBundle:Report")->getFileWithUser($id);
    	if (!$file){
    		throw $this->createNotFoundException('Unable to find the file with id: ' . $id . '.');
    	}
    	
    	//Check if the user owns the file or if his role is lesser than ROLE_STAFF (weight 2)
    	if ($user->getId() != $file->getUser()->getId() && $user->getRole()->getNumber() < 2){
    		$this->get('session')->getFlashBag()->add(
            	'warning',
            	'You cannot download a file that isn\'t yours'
    		);
    		return $this->redirect($this->generateUrl('homepage_client'), 301);
    	}
    	
    	//Create download response
        $path = $file->getAbsolutePath();
        $content = file_get_contents($path);

        $response = new Response();

        $response->headers->set('Content-Type', $file->getMimeType());
        //Generate a filename based on the attribute name of Report $file
        $response->headers->set('Content-Disposition', 'attachment; filename='.$file->generateName());

        $response->setContent($content);
        return $response;
    }
    
}