<?php

namespace Lightnest\DistributionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Lightnest\UserBundle\Entity\User;
use Lightnest\UserBundle\Entity\UserRepository;
use Lightnest\UserBundle\Form\Type\UserType;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="homepage_admin")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
    	return array();
    }
    
    /**
     * @Route("/user/new", name="admin_user_new")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {
    	//Create form and set role to ROLE_STAFF
    	$em = $this->getDoctrine()->getManager();
    	$user = new User(true);
    	$role = $em->getRepository("LightnestUserBundle:Role")->findOneByRole("ROLE_STAFF");
    	$user->setRole($role);
    	$form = $this->createForm(new UserType(), $user);
    	
    	$form->handleRequest($request);
    	
    	if ($form->isValid()) {
    		//Encode the password with the encoder define in config.yml
    		$encoder = $this->get('security.encoder_factory')->getEncoder($user);
    		$passwordNEncode = $user->getPassword();
    		$password = $encoder->encodePassword($passwordNEncode, $user->getSalt());
    		$user->setPassword($password);
    		
    		$em->persist($user);
    		$em->flush($user);
    		
    		try {
    			//Send email to the new user;
    			$message = \Swift_Message::newInstance()
        			->setSubject('Welcome on Lightnest SEO Portal')
        			->setFrom('staff-lightnest-seo-portal@lightnest.com')
        			->setTo($user->getUsername())
        			->setContentType('text/html')
        			->setBody($this->renderView('LightnestDistributionBundle:Admin:email.html.twig', array(
        				'user'		=> $user->getUsername(),
        				'password'	=> $passwordNEncode,
        				'host'		=> $this->container->getParameter('email_host')
	        		)))
	        	;
    			$this->get('mailer')->send($message);
    		} catch (Exception $e) {
    			error_log($e->getMessage());
    		}
    		
    		//Set flash to inform the user that this action has been succeed
    		$this->get('session')->getFlashBag()->add(
            	'notice',
            	'The user ' . $user->getUsername() . ' has been created'
    		);
    		return $this->redirect($this->generateUrl('admin_user_list'), 301);
    	}
    	
    	return array(
    		'form' => $form->createView(),
    	);
    }
    
    /**
     * @Route("/user/{id}/delete", name="admin_user_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
    	//Try to find the user
    	$em = $this->getDoctrine()->getManager();
    	$user = $em->getRepository("LightnestUserBundle:User")->getUserWithJoin($id);
    	if (!$user){
    		throw $this->createNotFoundException('Unable to find the User id: ' . $id . '.');
    	}
    	
    	//Prohibit the removal of an admin user
    	if ($user->getRole()->getNumber()>= 3){
    		$this->get('session')->getFlashBag()->add(
            	'warning',
            	'You can not delete an admin'
    		);
    		return $this->redirect($this->generateUrl('admin_user_list'));
        	
    	}
    	
    	$em->remove($user);
    	$em->flush();
    	$this->get('session')->getFlashBag()->add(
           	'notice',
           	'The user ' . $user->getUsername() . ' has been deleted.'
    	);
    	
    	return $this->redirect($this->generateUrl('admin_user_list'));
    }
    
    /**
     * @Route("/user/list", name="admin_user_list")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listAction()
    {    	
    	$query = $this->getDoctrine()->getRepository('LightnestUserBundle:User')->getAllUsersWithJoinQuery();
    	
    	$paginator  = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
        	$query,
        	$this->get('request')->query->get('page', 1)/*page number*/,
        	10/*limit per page*/
    	);
    	
    	return array(
    		'pagination' => $pagination
    	);
    }
}