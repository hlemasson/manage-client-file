<?php

namespace Lightnest\DistributionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Lightnest\DistributionBundle\Entity\FileRepository;
use Lightnest\UserBundle\Entity\User;

/**
 * Lightnest\DistributionBundle\Entity\Report
 * 
 * @ORM\Table(name="lightnest_report")
 * @ORM\Entity(repositoryClass="Lightnest\DistributionBundle\Entity\ReportRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, name="name")
     * @Assert\NotBlank
     * @Assert\Regex("/^[a-zA-Z]+(([a-zA-Z0-9]|[ \t]|-)*[a-zA-Z0-9])?$/")
     */
    private $name;
    
    /**
     * @ORM\Column(type="integer", name="size")
     */
    private $size;

    /**
     * @Assert\File(maxSize="6000000",
     * mimeTypes={"application/pdf",
     * 		"application/x-pdf",
     * 		"application/msword",
     * 		"application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
     * mimeTypesMessage="Choose a valid PDF or docx file.") 
     */
    private $file = null;
    
    /**
     * @ORM\Column(type="string", name="filename", length=255, nullable=false)
     */
    private $filename = null;

    /**
     * Stock old filename if we upload a new file 
     */
    private $old_filename;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, name="mime_type")
     */
    private $mimeType;
    
	/**
	 * @ORM\ManyToOne(targetEntity="Lightnest\UserBundle\Entity\User", inversedBy="files")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	private $user;

	
	/**
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}
	
	/**
	 * @param User
	 * @return Report
	 */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	
	public function getAbsolutePath()
    {
        return null === $this->filename ? null : $this->getUploadRootDir().'/'.$this->filename;
    }

    public function getWebPath()
    {
        return null === $this->filename ? null : $this->getUploadDir().'/'.$this->filename;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du r�pertoire o� les documents upload�s doivent �tre sauvegard�s
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se d�barrasse de � __DIR__ � afin de ne pas avoir de probl�me lorsqu'on affiche
        // le document/image dans la vue.
        return 'uploads/files/' . $this->getUser()->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate() 
     */
    public function preUpload()
    {
    	if ($this->file !== null){
    		//Stock previous filename to delete it if the entity is flushed
    		if (!$this->filename === null){
    			$this->old_filename = $this->filename;
    		}
    		$this->size = $this->file->getClientSize();
    		$this->mimeType = $this->file->getClientMimeType();
    		//Generate a filename based on name entered by staff user
    		$this->filename = sha1(uniqid(mt_rand(), true)) . '.' . $this->file->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	//In case a file isn't uploaded
    	if ($this->file === null){
    		return;
    	}
    	
    	//Remove the old file
    	if ($this->old_filename != null){
    		@unlink($this->old_filename);
    	}
    	
    	//Move the new file
    	$this->file->move($this->getUploadRootDir(), $this->filename);
    	
    	unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()){
    		@unlink($file);
    	}
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Report
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Report
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return Report
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return integer 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return Report
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string 
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }
    
    /**
     * Generate a filename for the download page
     * Based on $name and the $mimeType 
     * $return string
     */
    public function generateName()
    {
    	$name = preg_replace('/\s+/', '', ucwords($this->name));
    	switch ($this->mimeType){
    		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    			$name .= '.docx';
    			break;
    		case 'application/msword':
    			$name .= '.doc';
    			break;
    		case 'application/pdf':
    		case 'application/x-pdf':
    			$name .= '.pdf';
    			break;
    		default:
    			$name .= (false === $pos = strrpos($this->filename, '.')) ? '' : substr($this->filename, $pos);
    	}
    	return $name;
    }
}
