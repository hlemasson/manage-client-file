<?php

namespace Lightnest\DistributionBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Lightnest\DistributionBundle\Entity\Report;

/**
 * ReportRepository
 */
class ReportRepository extends EntityRepository
{
	/**
	 * Get a file with his user
	 * @param integer $id
	 * return null|File
	 */
	public function getFileWithUser($id)
	{
		$q = $this->createQueryBuilder('f')
			->select('f', 'u')
			->leftJoin('f.user', 'u')
			->where('f.id = :id')
			->setParameter('id', $id)
			->getQuery()
			;
		
		$result = $q->getResult();
		if (count($result) == 0){
			return null;
		}
		return $result[0];
	}
	
	public function getFilesQuery($userId)
	{
		$q = $this->createQueryBuilder('f')
			->select('f')
			->leftJoin('f.user', 'u')
			->where('u.id = :user_id')
			->setParameter('user_id', $userId)
			->getQuery();
			
		return $q;
	}
}
