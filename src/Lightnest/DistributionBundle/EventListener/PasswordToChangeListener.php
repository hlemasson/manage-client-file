<?php

namespace Lightnest\DistributionBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContext;

use Lightnest\UserBundle\Entity\User;

/**
 * Allow to force a user to be redirected on the user_change_password until he has not reset at least once his password
 */
class PasswordToChangeListener
{
	private $router;
    private $securityContext;

    public function __construct($router, SecurityContext $securityContext)
    {
    	$this->router = $router;
        $this->securityContext = $securityContext;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        //Check if there is a connected user
        if ($this->securityContext->getToken() && $this->securityContext->getToken()->getUser() !== 'anon.'){
    		$user = $this->securityContext->getToken()->getUser();
    		
    		//Check if a user have to change his password
    		if ($user->getMustChange()){
            	$route = $event->getRequest()->get('_route');
    			//Operate the redirection if the user not on the reset password route
            	if (strcmp($route, 'user_change_password') !== 0){
            			$url = $this->router->generate('user_change_password');
            			$event->setResponse(new RedirectResponse($url));
            	}
    		}
        }
    }
}